package br.com.mandae.api.webhook.order;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.mandae.api.webhook.order.controller.OrderWebHookController;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class OrderControllerTest {

	private MockMvc mvc;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(new OrderWebHookController()).build();
	}

	@Test
	public void getHello() throws Exception {
		String content = "{\"partnerOrderId\": \"ORDER0001\"," +
				"\"status\": \"success\"," +
				"\"jobId\": \"3afe3041-661e-431f-ba1d-46477ce1f6a2\"," +
				"\"orderId\": 123456}";

		System.out.println(content);
		mvc.perform(MockMvcRequestBuilders.post("/order/webhook")
				.content(content)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string(equalTo("success")));
	}
}
