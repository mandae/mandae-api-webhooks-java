package br.com.mandae.api.webhook.item.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mandae.api.webhook.item.beans.Item;

@RestController
@RequestMapping(value = "/item", consumes = "application/json;charset=UTF-8")
public class ItemWebHookController {

	@RequestMapping(value = "/webhook", method = RequestMethod.POST)
	public String webhook(@RequestBody final Item item) {
		System.out.println(item);
		return "success";
	}

}
