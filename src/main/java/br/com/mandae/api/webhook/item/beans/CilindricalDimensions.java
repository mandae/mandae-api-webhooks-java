package br.com.mandae.api.webhook.item.beans;

public class CilindricalDimensions implements Dimensions {
	
	private Float height;
	private Float diameter;
	private Float weight;
	private Double volume;
	
	@Override
	public String type() {
		return "cilindrical";
	}

	public Float getHeight() {
		return height;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	public Float getDiameter() {
		return diameter;
	}

	public void setDiameter(Float diameter) {
		this.diameter = diameter;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	@Override
	public String toString() {
		return "CilindricalDimensions [height=" + height + ", diameter=" + diameter + ", weight=" + weight
				+ ", volume=" + volume + ", type()=" + type() + "]";
	}

}
