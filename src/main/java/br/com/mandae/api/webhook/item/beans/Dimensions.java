package br.com.mandae.api.webhook.item.beans;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property="type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = CubicDimensions.class, name = "cubic"),
    @JsonSubTypes.Type(value = CilindricalDimensions.class, name = "cilindrical")
})
public interface Dimensions {

	String type();
	Number getWeight();
	Number getVolume();

}
