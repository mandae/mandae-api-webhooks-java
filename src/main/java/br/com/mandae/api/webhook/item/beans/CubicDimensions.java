package br.com.mandae.api.webhook.item.beans;

public class CubicDimensions implements Dimensions {
	
	private Float width;
	private Float height;
	private Float length;	
	private Float weight;
	private Float volume;
	
	@Override
	public String type() {
		return "cubic";
	}

	public Float getWidth() {
		return width;
	}

	public void setWidth(Float width) {
		this.width = width;
	}

	public Float getHeight() {
		return height;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	public Float getLength() {
		return length;
	}

	public void setLength(Float length) {
		this.length = length;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Float getVolume() {
		return volume;
	}

	public void setVolume(Float volume) {
		this.volume = volume;
	}

	@Override
	public String toString() {
		return "CubicDimensions [width=" + width + ", height=" + height + ", length=" + length + ", weight=" + weight
				+ ", volume=" + volume + ", type()=" + type() + "]";
	}

}
