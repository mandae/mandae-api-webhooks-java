package br.com.mandae.api.webhook.order.beans;

public class OrderStatus {

	private String status;
	private Number orderId;
	private String jobId;
	private String partnerOrderId;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getJobId() {
		return jobId;
	}

	public void setOrderId(Number orderId) {
		this.orderId = orderId;
	}

	public Number getOrderId() {
		return orderId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getPartnerOrderId() {
		return partnerOrderId;
	}

	public void setPartnerOrderId(String externalId) {
		this.partnerOrderId = externalId;
	}

	@Override
	public String toString() {
		return "OrderStatus [status=" + status + ", orderId=" + orderId
				+ ", jobId=" + jobId + ", partnerOrderId=" + partnerOrderId
				+ "]";
	}

}
