package br.com.mandae.api.webhook.order.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mandae.api.webhook.order.beans.OrderStatus;

@RestController
@RequestMapping(value = "/order")
public class OrderWebHookController {

	@RequestMapping(value = "/webhook", method = RequestMethod.POST)
	public String webhook(@RequestBody final OrderStatus orderStatus) {
		System.out.println(orderStatus);
		return "success";
	}

}
