package br.com.mandae.api.webhook.item.beans;

import java.util.Arrays;

public class Item {

	private Long id; // ID provided by Mandaê
	private String partnerItemId; // your item ID
	private String trackingCode; // code used for tracking this item
	private String trackingUrl; // url to track the order this item belongs to
	private Float price; // item price
	private Dimensions dimensions; // item dimensions. check the 'type' attribute to determine the correct type
	private String[] qrCodes; // Mandaê QR Codes

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPartnerItemId() {
		return partnerItemId;
	}

	public void setPartnerItemId(String externalId) {
		this.partnerItemId = externalId;
	}

	public String getTrackingCode() {
		return trackingCode;
	}

	public void setTrackingCode(String trackingCode) {
		this.trackingCode = trackingCode;
	}

	public String getTrackingUrl() {
		return trackingUrl;
	}

	public void setTrackingUrl(String trackingUrl) {
		this.trackingUrl = trackingUrl;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Dimensions getDimensions() {
		return dimensions;
	}

	public void setDimensions(Dimensions dimensions) {
		this.dimensions = dimensions;
	}

	public String[] getQrCodes() {
		return qrCodes;
	}

	public void setQrCodes(String[] qrCodes) {
		this.qrCodes = qrCodes;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", partnerItemId=" + partnerItemId
				+ ", trackingCode=" + trackingCode + ", trackingUrl="
				+ trackingUrl + ", price=" + price + ", dimensions="
				+ dimensions + ", qrCodes=" + Arrays.toString(qrCodes) + "]";
	}

}
