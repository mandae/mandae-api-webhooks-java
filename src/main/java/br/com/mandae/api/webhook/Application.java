package br.com.mandae.api.webhook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.SerializationFeature;

@SpringBootApplication
public class Application {
	
	@Bean
	public Jackson2ObjectMapperBuilder jacksonObjectMapperSetup() {
		return Jackson2ObjectMapperBuilder
			.json()
			.featuresToEnable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
			.featuresToDisable(SerializationFeature.WRITE_ENUMS_USING_INDEX, SerializationFeature.WRITE_NULL_MAP_VALUES)
			.serializationInclusion(Include.NON_NULL)
			.simpleDateFormat("yyyy/MM/dd");
	}

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
